<?php
session_start();
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['productID'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				/*$modelImage = $_FILES['modelImage']['tmp_name'];
				$result = date("YmdHis");
				$path_info = pathinfo($_FILES['modelImage']['name']);
				$upload_file_name = $result."_0.".".$path_info['extension'];
				copy($modelImage,"../../img/uploaded/".$upload_file_name);*/
				
				$modelImage = $_FILES['modelImage']['tmp_name'];
				$result = date("YmdHis");//$date->format('YmdHis');
				$path_info = pathinfo($_FILES['modelImage']['name']);
				$upload_file_name = $result.'_0.'.$path_info['extension'];
				copy($modelImage,"../../images/uploaded/$upload_file_name");
		
				$data['productID']	=	$App->convert($_REQUEST['productID']);	
				$data['modelName']	= $App->convert($_REQUEST['modelName']);
				$data['modelImagePath'] = $App->convert("images/uploaded/".$upload_file_name);			
				
				//echo "productID: ".$data['productID'].", ModelName: ".$data['modelName'].", imagePath: ".$data['modelImagePath'];
				//die;	
				$db->query_insert(TABLE_PRODUCT_MODEL,$data);						
				$db->close();			
				$_SESSION['msg']="Model Details Added Successfully";					
				header("location:new.php");						
			}
				
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];  		    
		if(!$_POST['productID'])
			{	
				$_SESSION['msg']="Error, Invalid Details.";									
				header("location:edit.php?id=$editId");	
			}
			else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				if(!empty($_FILES['modelImage']['name']))
				{
					$selectModelImg = "select * from ".TABLE_PRODUCT_MODEL." where ID=".$editId;
					$resSelectModelImg = mysql_query($selectModelImg);
					$rowSelectModelImg = mysql_fetch_array($resSelectModelImg);
					$curModelImgPath = "../../".$rowSelectModelImg['modelImagePath'];
					unlink($curModelImgPath);
					$modelImage = $_FILES['modelImage']['tmp_name'];
					$result = date("YmdHis");//$date->format('YmdHis');
					$path_info = pathinfo($_FILES['modelImage']['name']);
					$upload_file_name = $result.'_0.'.$path_info['extension'];
					copy($modelImage,"../../images/uploaded/$upload_file_name");
					$data['modelImagePath'] = $App->convert("images/uploaded/".$upload_file_name."");
				}
				
				
				$data['productID']	=	$App->convert($_REQUEST['productID']);	
				$data['modelName']	=	$App->convert($_REQUEST['modelName']);			
					
				$db->query_update(TABLE_PRODUCT_MODEL,$data," ID='{$editId}'");								
				$db->close();
				
				$_SESSION['msg']="Model Details Updated successfully";					
				header("location:new.php");					
			}		
		
		break;		
	// DELETE SECTION
	case 'delete':		
				$id	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();	
				$selectModelDel = "select * from ".TABLE_PRODUCT_MODEL." where ID= ".$id."";
				$resModelDel = mysql_query($selectModelDel);
				$rowModelDel = mysql_fetch_array($resModelDel);
				$currentModelImgPath = "../../".$rowModelDel['modelImagePath'];
				unlink($currentModelImgPath);	
				$db->query("DELETE FROM `".TABLE_PRODUCT_MODEL."` WHERE ID='{$id}'");								
				$db->close(); 
				$_SESSION['msg']="Model Details Deleted Successfully";					
				header("location:new.php");					
		break;		
}
?>