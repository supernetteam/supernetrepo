<?php include("../adminHeader.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
	var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
		window.submit();
	}
	else
	{
		return false;
	}
}



</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
 ?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">PRODUCT MODELS</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>
          <div class="col-sm-4" >
            <form method="post">
              <div class="input-group">
                <input type="text" class="form-control"  name="name" placeholder="Model Name" value="<?php echo @$_REQUEST['name'] ?>">
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="type"></button>
                </span> </div>
            </form>
          </div>
        </div>
		 <?php	
			$cond="1";
			if(@$_REQUEST['name'])
			{			
				$cond=$cond." and model.modelName  like'%".$_POST['name']."%'";
			}
			
			?>
			<div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table class="table table_admin pagination_table view_limitter" >
                <thead>
                  <tr>
                    <th>SL No</th>
					<th>Model No</th>																
					<th>Product</th>																
					<th>Category</th>																
					<th>Thumbnail</th>																
                  </tr>
                </thead>
                <tbody>
						<?php 
						$i=1;
						$select1=mysql_query("select model.ID, model.productID, model.modelName, model.modelImagePath, prod.productCategoryID, prod.productName, pcat.productCategory from `".TABLE_PRODUCT_MODEL."` model, `".TABLE_PRODUCT."` prod, `".TABLE_PRODUCT_CATEGORY."` pcat where model.productID=prod.ID AND prod.productCategoryID=pcat.ID AND $cond ");
						//$select1=mysql_query("select * from `".TABLE_PRODUCT_MODEL."` where $cond order by ID desc");
						
						$number=mysql_num_rows($select1);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="10">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
							while($row=mysql_fetch_array($select1))
							{	
							$tableId=$row['ID'];
							?>
					  <tr>
						<td><?php echo $i; $i++; ?>
						  <div class="adno-dtls"> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> | <a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>
						  </div>
						</td>
						
						<td><?php echo $row['modelName']; ?></td>															
						<td><?php echo $row['productName']; ?></td>															
						<td><?php echo $row['productCategory']; ?></td>															
						<td><img src="../../<?= $row['modelImagePath']; ?>" width="100" /></td>	
					  </tr>
					  <?php }
					  }
					  ?>                  
                </tbody>
              </table>
            </div>
          </div>          
        </div>
        <div class="row">
            <div class="col-lg-12 page_numbers text-center">
                <div class="btn-group pager_selector">
                </div>
            </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">ADD NEW MODEL</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-sm-6">             
                    <div class="form-group">
                      	<label for="name">Product:*</label>	
                        <select name="productID" class="form-control" required="required">
                        	<option>Select</option>
                        	<?php
                        		$selectPro = "select * from ".TABLE_PRODUCT."";
                        		$resPro = mysql_query($selectPro);
                        		while($rowPro = mysql_fetch_array($resPro))
                        		{
								?>
									<option value="<?= $rowPro['ID']; ?>"><?= $rowPro['productName']; ?></option>		
								<?php	
								}
								
                        	?>
                        </select>
                    </div>
                    <div class="form-group">
                    	<label>Model Name:*</label>
                    	<input class="form-control" name="modelName" required="required" />
                    </div>
                    <div class="form-group">
                    	<label>Product Image:*</label>
                    	<input name="modelImage" id="newImage" type="file" required="required" class="file-loading" accept="image/*">
                    </div>                   				
                   </div>                  
                </div>  
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
