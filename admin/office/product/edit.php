<?php include("../adminHeader.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];
	$tableEdit=mysql_query("SELECT * FROM `".TABLE_PRODUCT."` WHERE ID='$editId'");	
	$editRow=mysql_fetch_array($tableEdit);
?>

 
      <!-- Modal1 -->
      <div>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">PRODUCT DETAILS</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">
                <div class="row">
                 <div class="col-sm-12">
                    <div class="form-group">
                      <label for="courseName">Category :*</label>
                      <select name="typeID" id="typeID" class="form-control2" required >
							<option value="">Select</option>
							<?php 
									//$select2="select * from ".TABLE_PRODUCT_CATEGORY."";
									$select2 = "select * from ".TABLE_PRODUCT_CATEGORY."";
									$res2=mysql_query($select2);
									while($row2=mysql_fetch_array($res2))
									{										
									?>
									<option value="<?php echo $row2['ID']?>" <?php if($editRow['productCategoryID']== $row2['ID']){?> selected="selected"<?php }?>><?php echo $row2['productCategory']?></option>
									<?php 									
									}
							?>				
						</select>
                    </div>                   
                    <div class="form-group">
                      <label for="countType">Product Name: </label>
                      <input type="text" class="form-control2" name="name" id="name" value="<?php echo $editRow['productName'] ?>">
                    </div>  
                    <div class="form-group">
                        <label for="countType">Current Thumbnail: </label><br />
                    	<img src="../../<?php echo $editRow['thumbnailPath'] ?>" alt="" width="250" />
                    </div>  
                    <div class="form-group">
                      <label for="name">Update Thumbnail: (290*290)</label>
                      <input id="image1" name="productThumb" type="file" class="file-loading" accept="image/*">
                    </div>
                    <div class="form-group">
                    	<textarea name="description" id="tinyText" ><?= $editRow['description']; ?></textarea>
                    </div>
                    <div class="form-group">
                    	<input type="checkbox" name="hmFeat" value="1" <?php if($editRow['hmFeat'] == 1){ echo 'checked'; } ?> />
                    	<label>Add to home page</label>
                    </div>
					</div> 					                                     
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
  
  <script>
  tinymce.init({
    selector: '#tinyText'
  });
  </script>
<?php include("../adminFooter.php") ?>
