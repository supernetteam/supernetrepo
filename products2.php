<?php
//database server
define('DB_SERVER', "localhost");
define('DB_USER', "root");
define('DB_PASS', "");
//database name
define('DB_DATABASE', "supernet");

//smart to define your table names also
define('TABLE_LOGIN', "tbllogin");
define('TABLE_PRODUCT_CATEGORY', "productcategory");
define('TABLE_PRODUCT', "product");
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);

$category = $_REQUEST['category'];


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Supernet</title>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href="css/owl.carousel.css" rel="stylesheet" />
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="bg">
<div id="bannermain">
  <div id="headermain">
    <div class="white-area"><img src="img/hd-white.png" alt=""></div>
    <div class="container">
      <div class="row"> 
        <!--<div class="col-md-3 logobox">
                         
                        </div>-->
        <div class="col-md-8  headerleft clearfix"> <a href="index.html" class="logo"></a>
          <div id="navigation">
            <nav class="navbar" role="navigation"> 
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <!--<a class="navbar-brand img-responsive" href="#"class="logo">gdg</a>--> 
              </div>
              
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                  <li><a href="index.html">Home</a></li>
                  <li><a href="about.html">About Us</a></li>
                  <li><a href="products.php">Products</a></li>
                  <li><a href="contact.html">Contact Us</a></li>
                </ul>
              </div>
              <!-- /.navbar-collapse --> 
            </nav>
          </div>
        </div>
        <div class="col-md-4 headerright">
          <div class="headermsg"> <a href="mailto:sales@supernetme.com">sales@supernetme.com</a> </div>
          <div class="headermob"> +97143519739 </div>
        </div>
      </div>
    </div>
  </div>
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active"> <img src="img/banner1.jpg" alt=""> </div>
      <div class="item"> <img src="img/banner2.jpg" alt=""> </div>
      <div class="item"> <img src="img/banner3.jpg" alt=""> </div>
    </div>
    
    <!-- Controls --> 
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
</div>
<div class="container">
  <div id="homeservicelinkarea" class="clearfix">
    <ul class="clearfix">
      <li class="darkbluebg"> <a href="cloud.html">
        <div class="servicewhiteicon1"></div>
        Cloud </a> </li>
      <li> <a href="Smart-office.html">
        <div class="servicewhiteicon2"></div>
        Smart office </a> </li>
      <li class="darkbluebg"> <a href="Sme-solutions.html">
        <div class="servicewhiteicon3"></div>
        Sme solutions </a> </li>
      <li> <a href="Professional-service.html">
        <div class="servicewhiteicon4"></div>
        Professional service </a> </li>
    </ul>
  </div>
  <div class="container-fluid">
    <div id="enquirymain">
      <div class="enquirybtn clearfix"> <span class="phoneicon"></span>
        <h4> QUICK<br/>
          CONTACT</h4>
      </div>
      <form action="qsend.php" class="enquiryform" method="post">
        <div class=" clearfix">
          <div class="form-group">
          <input type="text" class="form-control" id="name" name="name" pattern="[a-zA-Z][a-zA-Z.]+" title="Name" required placeholder="Name">
        </div>
        <div class="form-group">
          <input type="tel" class="form-control" id="phone" name="phone" pattern="[0-9,+][0-9]+" title="Phone No" required placeholder="Phone" >
        </div>
        <div class="form-group">
          <input type="email" class="form-control" id="email" name="email" required placeholder="E-mail" title="E-mail">
        </div>
        <div class="form-group">
          <textarea class="form-control2" rows="4" name="message"></textarea>
        </div>
          <span class="submit">
          <input class="btn" type="submit" value="Send">
          </span> </div>
      </form>
    </div>
  </div>  
</div>

<!--Products-->
<div class="container">
	
		
		
		<?php
		//$selectProduct = "select pro.ID as proID, pro.productCategoryID, pro.productName, pro.thumbnailPath, pro.description, pcat.ID as catID, pcat.productCategory  from `".TABLE_PRODUCT."` pro, `".TABLE_PRODUCT_CATEGORY."` pcat where pro.productCategoryID = pcat.ID";
		$selectProduct = "select * from ".TABLE_PRODUCT_CATEGORY;
		//echo $selectProduct;
		
		$queryPro = mysqli_query($connection, $selectProduct);
		if(mysqli_num_rows($queryPro) > 0){
			$i = 0;
			while($rowP = mysqli_fetch_array($queryPro)){
				$productArray[$i][0] = $rowP['ID'];
				$productArray[$i][1] = $rowP['productCategory'];
				$i++;
			}
			$a = 0;
			for($j = 0; $j<$i; $j++){
				$pcatID = $productArray[$j][0];
				$pcatName = $productArray[$j][1];
				//echo ($productArray[$j][0]);
				$selectProdFinal = "select * from`".TABLE_PRODUCT."` where productCategoryID = $pcatID";
				//echo $selectProdFinal;
				$queryProd_final = mysqli_query($connection, $selectProdFinal);
				?>
				<div class="pro_section">
				<?php
				while($rowProdFinal = mysqli_fetch_array($queryProd_final)){
					if($a != $pcatID){
					?>
					<div class="pro_head">
						<div class="pro_head_inner">
							<h3><?= $pcatName; ?></h3>
						</div>
					</div>
					<?php	
					$a = $pcatID;
					}
					?>
					
					<div class="product_box" data-toggle="modal" data-target="#myModal" data-description="<?php if($rowProdFinal['description']){ echo $rowProdFinal['description']; } ?>">
						<div class="product_inner">
							<div class="pro_img_box">
								<img src="admin/<?= $rowProdFinal['thumbnailPath']; ?>"/>
							</div>					
							<h3><?= $rowProdFinal['productName']; ?></h3>
							<div class="zoom_box"></div>
						</div>
					</div>
					
					<?php
				}
				
				?>
				<div class="bd_clear"></div>
				</div>
				
				
				
				<?php
				
			}
		}
		
	?>
	
	
	
	
	
	
	
	
	
	
	
	
	
</div>
 

<div id="footermain">
  <div class="container">
    <div class="row">
      <div class="col-sm-4"><a href="index.html"><img src="img/footerlogo.jpg"  alt=""/></a></div>
      <div class="col-sm-3">
        <div class="footeraddress"> PB No: 43976, <br/>
          Dubai , U.A.E </div>
      </div>
      <div class="col-sm-3">
        <div class="footeraddress"><!-- E-Mail:<a href="mailto:email@mail.com"> email@mail.com</a> <br/>--> 
          Phone : +97143519739<br/>
          Fax : +97143519759 </div>
      </div>
      <div class="col-sm-2">
        <ul class="list-inline social">
          <li><a href="#" class="fb"></a></li>
          <li><a href="#" class="gplus"></a></li>
          <li><a href="#" class="twt"></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <p>Copyright � 2015 Supernet. All Rights Reserved. </p>
        <div class="powered">Powered by<a href="http://www.bodhiinfo.com/" target="_blank" class="poweredlogo"></a></div>
      </div>
    </div>
  </div>
</div>


<button type="button" class="btn btn-info btn-lg product_trigger" data-toggle="modal" data-target="#myModal">Open Modal</button>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        		<img class="pro_pop_img" src="" />
        	</div>
        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        		<div class="description_wrap">
        			
        		</div>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script> 
<script>
	$(document).ready(function(){
		$('.product_box').click(function(){
			cur = $(this);
			var proHead = cur.find('h3').text();
			var proImg = cur.find('img').attr('src');
			var description = cur.data('description');
			//console.log(proHead+', '+proImg+', '+description);
			$('#myModal .modal-title').text(proHead);
			$('#myModal .pro_pop_img').attr('src', proImg);
			if(description != ""){
				$('#myModal .description_wrap').html(description);
			} else{
				$('#myModal .description_wrap').html("");
			}
		});
	});
</script>
<script src="js/owl.carousel.min.js"></script> 
<script>
		$('#brands').owlCarousel({
			loop:true,
			margin:10,
			responsiveClass:true,
			nav:false,
			autoplay:true,
			responsive:{
				0:{
					items:1
				},
				370:{
					items:2
				},
				600:{
					items:3
				},
				1000:{
					items:4,
					loop:true
				}
			}
		})
		
		var mq = window.matchMedia( "(max-width: 480px)" );
	if (mq.matches) {
		$('.enquirybtn').click(function(){
		window.location.href = "http://supernetme.com/contact.html#contactform";
		});
	}
	else {
		$('.enquirybtn').click(function(){
			$(this).parent('#enquirymain').toggleClass('open');
		});
	}
</script>
</body>
</html>