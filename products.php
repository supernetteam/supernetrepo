<?php
include('header.php');

$categoryID = $_REQUEST['category'];


?>


<!--Products-->
<div class="container innerarea" style="padding: 40px 15px;">
	<div class="category_head">
		<?php
			$selectCat = "select productCategory from ".TABLE_PRODUCT_CATEGORY." where ID = ".$categoryID;
			$resCat = mysqli_query($connection, $selectCat);
			if(mysqli_num_rows($resCat)){
				$rowCat = mysqli_fetch_array($resCat);
				?>
				<h3><?= $rowCat['productCategory']; ?></h3>
				<?php				
			}
		?>		
	</div>
		
		
	<?php
	$proSelect = "select * from ".TABLE_PRODUCT." where productCategoryID = ".$categoryID;
	$proRes = mysqli_query($connection, $proSelect);
	
	if(mysqli_num_rows($proRes) > 0){
		while($proRow = mysqli_fetch_array($proRes)){
		?>
		
		<div class="product_box" data-product_id="<?= $proRow['ID']; ?>" data-description="<?php if($proRow['description']){ echo $proRow['description']; } ?>">
			<div class="product_inner">
			<div class="pro_img_box">
				<img src="admin/<?= $proRow['thumbnailPath']; ?>"/>
			</div>					
			<h3><?= $proRow['productName']; ?></h3>
			<div class="zoom_box" data-toggle="modal" data-target="#myModal"></div>
			<span class="enquiry_trigger" data-toggle="modal" data-target="#enquiry_form">Enquire Now</span>
			</div>
		</div>
		
		<?php
		}
	}
	
	?>
	
	
	
	
</div>
 

<button type="button" class="btn btn-info btn-lg product_trigger" data-toggle="modal" data-target="#myModal">Open Modal</button>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        		<img class="pro_pop_img" src="" />
        	</div>
        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        		<div class="description_wrap">
        			
        		</div>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="enquiry_form" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enquiry Form</h4>
      </div>
      <div class="modal-body">
        <form class="enquiry_frm" method="post" action="enquiry.php">
        	<input type="hidden" name="cat_ID" value="<?= $categoryID; ?>"/>
        	<input id="productID" type="hidden" name="productID"/>
        	<div class="form_block">
        		<input class="form-control" type="text" name="name" placeholder="Name" required />
        	</div>
        	<div class="form_block">
        		<input class="form-control" type="email" name="email" placeholder="Email ID" required />
        	</div>
        	<div class="form_block">
        		<input class="form-control" type="text" name="mobile" placeholder="Contact Number" />
        	</div>
        	<div class="form_block">
        		<input class="form-control" type="text" name="company_name" placeholder="Company Name"/>
        	</div>
        	<div class="form_block">
        		<textarea class="form-control" name="enquiry" placeholder="Your Enquiry"></textarea>
        	</div>
        	<div class="form_block">
        		<button type="submit" class="btn btn-success">Send</button>
        	</div>
        </form>
      </div>
    </div>

  </div>
</div>





<?php
include('footer.php');
?>