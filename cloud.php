<?php
include('header.php');
?>
<div class="innerarea" id="service">
  <div class="container">
    <div class="row">
      <div class="col-sm-4"> <img src="img/serviceimg1.jpg" class="img-responsive serviceimg" alt=""/> </div>
      <div class="col-sm-8">
        <h3>Cloud</h3>
        <p>The crux of a cloud service is based on the 3 crucial factors – Infrastructure, management & expertise. Supernet’s quality infrastructure support &unmatched proficiency, combined with the support of a passionate team makes us the ideal partner for providing cutting edge cloud solutions.</p>
        <p>From the start till the end, we are right there with you offering support & expertise to help your company take the next big leap.</p>
        <p>Our services range from fully managed cloud infrastructure, storage, maintenance & dedicated support. We will help you install a state of the art solution, for deployment & management of desktops, servers, networks etc.</p>
        <p>Supernet is already the preferred business partner for many companies, big or small, across the globe. Be it a finance organization or a global retail channel, we have the technological capability & scalability to cater to the varied needs of our clients.</p>
        <p>When you work with Supernet, we ensure that your priorities are our priorities. All our clients are offered a dedicated cloud expert to make sure all your requirements are implemented. </p>
        <p>So leave your worries & let us help you achieve your goals.</p>
        <h6>What we Offer</h6>
        <ul class="clearfix">
          <li>BYOD/BYOC</li>
          <li>XEN APP & VDI</li>
          <li>VIRTUALIZATION</li>
          <li>SERVER ON CLOUD</li>
          <li>STORAGE ON CLOUD</li>
          <li>CLOUD WIRELESS</li>
          <li>CLOUD MONITOR & DESKTOP</li>
          <li>PRIVATE CLOUD SERVICE.</li>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php
include('footer.php');
?>