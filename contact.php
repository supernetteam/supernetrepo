<?php
include('header.php');
?>
<div class="innerarea">
  <div class="container">
    <h3>Contact Us</h3>
    <div id="addressarea">
      <div class="row">
        <div class="col-sm-7">
          <div class="row">
            <div class="col-sm-6">
              <p>PB No: 43976, <br/>
                Dubai , U.A.E <br/>
            </div>
            <div class="col-sm-6">
              <p> Email : <a href="mailto:sales@supernetme.com">sales@supernetme.com</a><br/>
                Phone : +97143519739<br/>
                Fax : +97143519759<br/>
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-5">
          <ul class="list-inline social">
            <li><a href="#" class="fb"></a></li>
            <li><a href="#" class="twt"></a></li>
            <li><a href="#" class="gplus"></a></li>
          </ul>
        </div>
      </div>
    </div>
    <form action="qsend.php" class="contactform" method="post" id="contactform">
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label for="name" class="label">Name*</label>
            <input type="text" class="form-control" id="name2" name="name" pattern="[a-zA-Z][a-zA-Z.]+" title="Name" required>
          </div>
          <div class="form-group">
            <label for="email" class="label">E-Mail*</label>
            <input type="email" class="form-control" id="email2" name="email" title="E-mail" required>
          </div>
          <div class="form-group">
            <label for="tel" class="label">Phone*</label>
            <input type="tel" class="form-control" id="phone" name="phone" pattern="[0-9,+][0-9]+" title="Phone No" required>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label">Message</label>
            <textarea class="form-control2" rows="3"  name="message"></textarea>
          </div>
          <span class="submit">
          <input class="btn" type="submit" value="Send">
          </span> </div>
      </div>
    </form>
  </div>
</div>
<div id="map">
  <div class="Flexible-container">
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1804.1598700080349!2d55.2910575!3d25.2598274!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjXCsDE1JzM2LjAiTiA1NcKwMTcnMjUuMiJF!5e0!3m2!1sen!2sin!4v1438085256676" width="800" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
</div>
<?php
include('footer.php');
?>