<?php
include('header.php');
?>
<div class="innerarea" id="about">
  <div class="container">
    <h3>About Us</h3>
    <div class="row">
      <div class="col-sm-4 col-sm-push-8"> <img src="img/aboutimg1.jpg" class="img-responsive aboutimg1"  alt=""/> </div>
      <div class="col-sm-8 col-sm-pull-4">
        <p>Supernet is proud to be one of the most successful & highly reputed IT service providers in the industry, delivering quality end to end support to companies across the globe. Be it cloud services, IT infrastructure, office automation, software development or smart communication systems, we ensure that your business requirements are well taken care of.</p>
        <p>We understand how crucial technology can be for success of your business. This is why we take an extra effort to listen to our clients thoroughly & identify their problems before deciding on the technology to be implemented to overcome various business challenges. Our services are extremely flexible & can be tailored to meet your exact requirements.</p>
        <p></p>
      </div>
    </div>
    <div class="row aboutbtm">
      <div class="col-sm-3"> <img src="img/aboutimg2.jpg" class="img-responsive aboutimg2"  alt=""/> </div>
      <div class="col-sm-9">
        <p>We let our clients make use of our expertise & infrastructure to achieve their business goals in minimum time. Deployment & maintenance of your own in-house solution comes with its own problems. More often you are left with no time to manage these systems in place, which is why, Super net is sought by our clients as a perfect IT partner for any business. </p>
      </div>
    </div>
  </div>
</div>
<?php
include('footer.php');
?>