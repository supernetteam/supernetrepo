-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 05, 2017 at 07:32 AM
-- Server version: 5.5.54-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bodhiinf_supernet`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `ID` int(11) NOT NULL,
  `productCategoryID` int(11) NOT NULL DEFAULT '0',
  `productName` varchar(50) NOT NULL DEFAULT '0',
  `thumbnailPath` varchar(50) NOT NULL DEFAULT '0',
  `description` varchar(1000) DEFAULT NULL,
  `hmFeat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`ID`, `productCategoryID`, `productName`, `thumbnailPath`, `description`, `hmFeat`) VALUES
(47, 9, 'AVAYA IP PHONE 1600', 'images/uploaded/20151231214311_0.JPG', '<h3>Two way speaker phones</h3>\r\n<p>&nbsp;</p>\r\n<p>The avaya 1600 are great invention into the world which are value priced and designed to meet todays communication needs in a low cost package&nbsp;</p>', 0),
(50, 8, 'DATAMAX E4205', 'images/uploaded/20151231222346_0.JPG', '<ul>\r\n<li>\r\n<h4>Direct thermal, thermal transfer(optional)</h4>\r\n</li>\r\n<li>\r\n<h4>Available in four distinct models: Basic,Advanced, Professional+</h4>\r\n</li>\r\n<li>\r\n<h4>4.25\'\'(108mm)@203dpi(8dots/mm)</h4>\r\n</li>\r\n<li>\r\n<h4>203 dpi[8 dots/mm],300 dpi[12 dots/mm] optional resolution.</h4>\r\n</li>\r\n</ul>', 0),
(51, 4, 'HP 280 G1 MICROTOWER PC', 'images/uploaded/20160101123847_0.jpg', '<ul>\r\n<li>\r\n<h4>Maximum memory: 16 GB DDR3-1600 SDRAM</h4>\r\n</li>\r\n<li>\r\n<h4>Memory Slots:2 UDIM</h4>\r\n</li>\r\n<li>\r\n<h4>Internal memory: 500GB 1TB SATA</h4>\r\n</li>\r\n<li>\r\n<h4>Dimension : 6.5X 13.98 X 14.13\'\'</h4>\r\n</li>\r\n<li>\r\n<h4>Weight : 7.05 Kg</h4>\r\n</li>\r\n</ul>', 0),
(52, 4, 'LENOVO THINKCENTRE E73', 'images/uploaded/20160101125310_0.jpg', '<ul>\r\n<li>\r\n<h4>Graphics: Integrated 2013 Intel HD</h4>\r\n</li>\r\n<li>\r\n<h4>Storage : 500Gb,1Tb,2TB.</h4>\r\n</li>\r\n<li>\r\n<h4>Memory : Upto 16 Gb,1600 MHz</h4>\r\n</li>\r\n<li>\r\n<h4>Dimensions: 400mmX160mmX397mm</h4>\r\n</li>\r\n</ul>', 1),
(53, 9, 'PANASONIC KX-TES 824 PBX', 'images/uploaded/20160101130535_0.jpg', '<ul>\r\n<li>\r\n<h4>Hybrid system.</h4>\r\n</li>\r\n<li>\r\n<h4>Upto 8 CO lines and 24 Extensions.</h4>\r\n</li>\r\n<li>\r\n<h4>Direct inward system access with message.</h4>\r\n</li>\r\n<li>\r\n<h4>Uniform call distribution with message.</h4>\r\n</li>\r\n<li>\r\n<h4>Caller ID display on SLT</h4>\r\n</li>\r\n</ul>', 0),
(54, 9, 'PANASONIC KX-TT7730 Phone', 'images/uploaded/20160101131031_0.jpg', '<ul>\r\n<li>\r\n<h4>Alphanumeric display.</h4>\r\n</li>\r\n<li>\r\n<h4>Programmable keys with Dual core LED.</h4>\r\n</li>\r\n<li>\r\n<h4>Feature access keys for LED.</h4>\r\n</li>\r\n<li>\r\n<h4>Hands free speech.</h4>\r\n</li>\r\n<li>\r\n<h4>Voice call.</h4>\r\n</li>\r\n</ul>', 0),
(55, 9, 'PANASONIC KX-TG3611 DORDLESS PHONE', 'images/uploaded/20160101131921_0.jpg', '<ul>\r\n<li>\r\n<h4>Caller ID compatable.</h4>\r\n</li>\r\n<li>\r\n<h4>Light Up Indicator.</h4>\r\n</li>\r\n<li>\r\n<h4>Dital handset Speaker phone</h4>\r\n</li>\r\n</ul>', 1),
(56, 9, 'AVAYA IP500 V2', 'images/uploaded/20160101132406_0.jpg', '<ul>\r\n<li>\r\n<h4>Backwards compatable with IP500.</h4>\r\n</li>\r\n<li>\r\n<h4>4 card slots compatible with existing IP500 cards as well as new Release 8.0 cards</h4>\r\n</li>\r\n<li>\r\n<h4>SD Card required, acts as Feature Key</h4>\r\n</li>\r\n<li>\r\n<h4>Operates in standard IP Office mode and Essential Edition PARTNER mode</h4>\r\n</li>\r\n</ul>', 0),
(57, 9, 'YEASTER MYPBX SOHO', 'images/uploaded/20160101132819_0.jpg', '<ul>\r\n<li>\r\n<h4>Maximum number of users : 32.</h4>\r\n</li>\r\n<li>\r\n<h4>Maximum concurrent calls : 15.</h4>\r\n</li>\r\n<li>\r\n<h4>Maximum analog ports : 4.</h4>\r\n</li>\r\n<li>\r\n<h4>LAN :1 10/100 Base-T Ethernet</h4>\r\n</li>\r\n</ul>', 0),
(58, 9, 'NEC SL1000 IP-PBX', 'images/uploaded/20160101133317_0.jpg', '<ul>\r\n<li>\r\n<h4>Room monitoring from outside.</h4>\r\n</li>\r\n<li>\r\n<h4>Warning message during night mode.</h4>\r\n</li>\r\n<li>\r\n<h4>Remote inspection with auto emergency call.</h4>\r\n</li>\r\n<li>\r\n<h4>Built in auto answer.</h4>\r\n</li>\r\n<li>\r\n<h4>TDM/IP coverage.</h4>\r\n</li>\r\n</ul>', 0),
(59, 9, 'CISCO SPA502G', 'images/uploaded/20160101134535_0.jpg', '<ul>\r\n<li>\r\n<h4>One voice line.</h4>\r\n</li>\r\n<li>\r\n<h4>Menu driven user interface.</h4>\r\n</li>\r\n<li>\r\n<h4>Shared line appearance.</h4>\r\n</li>\r\n<li>\r\n<h4>Caller ID name and number.</h4>\r\n</li>\r\n<li>\r\n<h4>Outbound caller ID blocking.</h4>\r\n</li>\r\n</ul>', 0),
(60, 9, 'AVAYA 9608 IP PHONE', 'images/uploaded/20160101135406_0.jpg', '<ul>\r\n<li>\r\n<h4>Monochrome display -3.2\'\'X 2.2\'\'.</h4>\r\n</li>\r\n<li>\r\n<h4>8 Buttons with dual LED.</h4>\r\n</li>\r\n<li>\r\n<h4>HArd buttons for phone, messages, contacts, history, name, home, navigation, cluster, headset, speaker.&nbsp;</h4>\r\n</li>\r\n<li>\r\n<h4>24 administrative buttons.</h4>\r\n</li>\r\n</ul>', 0),
(61, 9, 'NEC IP PHONE 12 TXH A', 'images/uploaded/20160101190638_0.jpg', '<ul>\r\n<li>\r\n<h4>Full duplex hands free.</h4>\r\n</li>\r\n<li>\r\n<h4>Equiped with RJ45 jacks for LAN and PC connections.</h4>\r\n</li>\r\n<li>\r\n<h4>Backlite 3 line 24 character display.</h4>\r\n</li>\r\n<li>\r\n<h4>User programmable function keys with Red/Green LEDs</h4>\r\n</li>\r\n</ul>', 0),
(62, 9, 'YEALINK SIP T38 PHONE', 'images/uploaded/20160101191929_0.jpg', '<ul>\r\n<li>TI Aries chipset and TI voice engine.</li>\r\n<li>Dual port Gigabit Ethernet&nbsp;</li>\r\n<li>Power over Ethernet.</li>\r\n<li>4.3\'\' TFT -LCD ,480 X 272 pixel,16.7 M colours.</li>\r\n<li>Color picture caller-ID,Screen Saver, Wallpaper.</li>\r\n</ul>', 1),
(63, 5, 'DELL INSPIRON 5558', 'images/uploaded/20160101192913_0.JPG', '<ul>\r\n<li>Upto 6th generation Intel core processors.Web pages, games and apps fire up fast.</li>\r\n<li>With upto 1TB of storage space,you have plenty of rooms to store your files.</li>\r\n<li>Great battery life upto 7 hours.</li>\r\n<li>Long range wifi : Newest&nbsp;802.11 AC technology increses the wifi range.</li>\r\n</ul>', 0),
(64, 5, 'HP PAVILION DV7T', 'images/uploaded/20160101193533_0.JPG', '<ul>\r\n<li>Sleek, futuristic \'liquid metal\' finish.</li>\r\n<li>Display : 17.0\'\' 1680 X 1050 pixels resolution.</li>\r\n<li>Primary graphics chipset : Nvidia GeForce 9600M GT.</li>\r\n<li>Battery life : 3 hours 30 minutes.</li>\r\n</ul>', 0),
(65, 5, 'LENOVO Y50 -70', 'images/uploaded/20160101194532_0.jpg', '<ul>\r\n<li>Display:15.6\'\' FHD IPS LED Backlight.</li>\r\n<li>Hard drive : Hybrid 1TB 5400 RPM + 8GB SSHD.</li>\r\n<li>Graphics : Nvidia GeForce GTX 960M 2GB.</li>\r\n<li>Bluetooth version 4.0.</li>\r\n<li>Battery: 4 cells 54 Watt Hour lithium -ion.</li>\r\n</ul>', 1),
(66, 5, 'DELL INSPIRON 3542', 'images/uploaded/20160101195702_0.JPG', '<ul>\r\n<li>Upto 8GB single Channel DDR3 SDRAM at 1600MHZ - 1 DIMMS</li>\r\n</ul>', 0),
(67, 8, 'ZEBRA LABEL PRINTER GC420T', 'images/uploaded/20160101210743_0.jpg', '<ul>\r\n<li>Resolution: 203 dpi/ 8 dots per minute.</li>\r\n<li>Memory: 8Mb flash,8 Mb SDRAM.</li>\r\n<li>Print width :4.09\'/104mm.</li>\r\n<li>Print length:39\'\'/990mm</li>\r\n<li>MAximum media roll size: 5\'\'/127mm O.D on a 1.00\'\'/25.4 mm,1.5\'\'/38mm ID core&nbsp;</li>\r\n</ul>', 1),
(68, 8, 'SYMBOL BARCODE SCANNER LS2208', 'images/uploaded/20160101211243_0.jpg', '<ul>\r\n<li>Dimensions: 6\'\'H X 2.5\'\' W X 3.34\'\' D.</li>\r\n<li>Weight : 5.29oz /150gm.</li>\r\n<li>Color: Cash register white or twilight black.</li>\r\n<li>Light source : 650 nm visible laser code.</li>\r\n<li>Print contrast : 20% minimum reflective differences</li>\r\n</ul>', 1),
(69, 8, 'HP RP7800 POS SYSTEM', 'images/uploaded/20160101213115_0.jpg', '<ul>\r\n<li>Processor:Inter core i3.</li>\r\n<li>Chipset: Intel Q67 Express.</li>\r\n<li>Memory: 4GB 1600MHz DDR3 SDRAM &nbsp;</li>\r\n<li>Dimensions: 36.6 x4.3 x 4 cm,17\'\' display.</li>\r\n<li>Network interface: Integrated intel 82579 LM Gigabit Ethernet Network Connection.</li>\r\n</ul>', 1),
(70, 8, 'ANTI SHOPLIFTING TAGS', 'images/uploaded/20160101221319_0.jpg', '<ul>\r\n<li>58 KHz AM</li>\r\n<li>Supertag series detacher.</li>\r\n<li>All 59 Khz AM system.</li>\r\n</ul>', 0),
(71, 8, 'HP RP3000 POS SYTEM', 'images/uploaded/20160101222342_0.jpg', '<ul>\r\n<li>Chipset: Intel 94GC with ICH7.</li>\r\n<li>Memory : DDR2 Synch RAM with non ECC memory.</li>\r\n<li>Support upto 2 GB of DDR2 synch DRAM.</li>\r\n<li>Graphics: Integrated Intel graphics media accelerator 950.</li>\r\n</ul>', 1),
(72, 8, 'DATALOGIC QW2100 BARCODE SCANNER', 'images/uploaded/20160101223110_0.jpg', '<ul>\r\n<li>Wide scan angle.</li>\r\n<li>Laser-like thinner and extended scan lines.</li>\r\n<li>Drop resistence to 1.5 m/50 sq ft.</li>\r\n<li>Water and particulate sealing rating :IP42.</li>\r\n</ul>', 0),
(73, 8, 'DATALOGIC COBALTO CO5300 LASER SCANNER', 'images/uploaded/20160101223734_0.jpg', '<ul>\r\n<li>\'Ring Of Light\' visual good-read confirmation.</li>\r\n<li>Configurable polyphonic speaker for customized audio good read confirmation.</li>\r\n<li>Adjustable scan head with 30 dgree tilting ability for larger items.</li>\r\n<li>Capacitive trigger for single line targeting applications such as price look up lists.</li>\r\n<li>Ergonomic rubber finishing grips are ideal for handheld operations.</li>\r\n</ul>', 1),
(74, 8, 'CASH DRAWER', 'images/uploaded/20160101224317_0.jpg', '<ul>\r\n<li>Highly reliable.</li>\r\n<li>Easy to operate.</li>\r\n<li>Highly recommended designs.</li>\r\n</ul>', 0),
(75, 8, 'RIBBONS FOR LABELS', 'images/uploaded/20160101225635_0.jpg', '<ul>\r\n<li>Ribbons coming in all dimensions.</li>\r\n</ul>', 0),
(76, 8, 'STAR SP 700 PRINTER RIBBON', 'images/uploaded/20160101225959_0.jpg', '', 0),
(77, 8, 'MAGELLAN 8400 DESK SCANNERS', 'images/uploaded/20160101230534_0.jpg', '<ul>\r\n<li>&nbsp;Aggressive and ergonomic 360 degree 5 sided scanning .</li>\r\n<li>Compatible with Mettler, Bizerba and other adaptive scales</li>\r\n<li>Optional value added features.</li>\r\n</ul>', 1),
(78, 5, 'HP 250 G4 NOTEBOOK', 'images/uploaded/20160102172406_0.JPG', '<ul>\r\n<li>Screen Size: 15.6\'\'.</li>\r\n<li>Dimensions: 38.43 X 24.46 X 2.43 cm.</li>\r\n<li>Weight : Starting at 2.1Kgs.</li>\r\n<li>Maximum memory : 8GB DDR3L -1600 SDRAM.</li>\r\n</ul>', 0),
(79, 5, 'TOSHIBA R30 A1040', 'images/uploaded/20160102173222_0.JPG', '<ul>\r\n<li>Screen Size: 13\'\'.</li>\r\n<li>Processor: 4rth Generation Intel i3,i5.</li>\r\n<li>Battery life: Upto 9 hours.</li>\r\n<li>Weights: Starting at 1.5 Kgs.</li>\r\n</ul>', 1),
(89, 5, 'ACER ASPIRE E1-572', 'images/uploaded/20160102174547_0.JPG', '<ul>\r\n<li>CPU: 1.6 GHz Intel core i5-4200U dual core processor.</li>\r\n<li>Hard drive speed: 5400 rpm.</li>\r\n<li>Native resolution: 1366 x 768.</li>\r\n<li>Graphics card: Intel HD graphics 4400.</li>\r\n<li>Touch pad size: 4.25 x 2.5\'\'.</li>\r\n</ul>', 0),
(90, 3, 'HP 300 GB SAS HARD DRIVE', 'images/uploaded/20160102175453_0.JPG', '', 1),
(91, 23, 'HP PROLIANT ML310 E', 'images/uploaded/20160102183732_0.JPG', '<ul>\r\n<li>Latest Intel Xeon E3-1200 v3 series processors.</li>\r\n<li>Dedicated iLo connection for faster and more secured data transmission.</li>\r\n<li>Windows server 2012 compatibility.</li>\r\n<li>Energy efficient Ethernet network adapter.</li>\r\n<li>Seamless network connectivity &nbsp;that extends the benefits of converged infra structure.</li>\r\n</ul>', 1),
(92, 23, 'IBM EXPRESS X3500M4', 'images/uploaded/20160102184600_0.JPG', '<ul>\r\n<li>Xeon 6C E5-2620 95W Processor.</li>\r\n<li>3 X 300GB HS 2.5IN SAS Hard drives.</li>\r\n<li>2 X 750W P/S tower</li>\r\n</ul>\r\n<p>&nbsp;</p>', 0),
(93, 23, 'HP PROLIANT DL380', 'images/uploaded/20160102185857_0.JPG', '<ul>\r\n<li>HP NVMe PCI e SSD s Support upto 1.6 TB.</li>\r\n<li>New Intel and NVIDIA GPUs.</li>\r\n<li>HP oneview for HP proliant rack Gen 9 Servers provides infra structure that reduces complexity with automation simplicity.</li>\r\n</ul>', 0),
(94, 23, 'IBM EXPRESS X3650M4', 'images/uploaded/20160102200315_0.JPG', '<ul>\r\n<li>Rack type server.</li>\r\n<li>Xeon 6C E-2620 Series processor.</li>\r\n<li>2.5\'\' SAS Hard drives.</li>\r\n<li>550 W P/S RACK.</li>\r\n</ul>', 1),
(95, 7, 'HP LASERJET PRO MFP M125NW', 'images/uploaded/20160105205141_0.JPG', '<ul>\r\n<li>HP Auto On/Off technology.</li>\r\n<li>E print/ Air print &amp; WiFi direct printing.</li>\r\n<li>A4 ; A3 ; Envelops ; Post cards.</li>\r\n<li>Network and USB connectivity.</li>\r\n<li>Up to 8000 prints per month.</li>\r\n</ul>', 0),
(96, 7, 'HP LASERJET PRO MFP 225DN', 'images/uploaded/20160105214837_0.JPG', '<ul>\r\n<li>Copy resolution: Upto 600 X 600 dpi black (text and graphics),color.</li>\r\n<li>Display : 2 line LCD(text).</li>\r\n<li>Fax resolution: Upto 300 X 300 dpi (Halftone enabled).</li>\r\n<li>Paper handling : 250 sheets input tray, 100 sheet output tray.</li>\r\n<li>Print upto 8000 pages.</li>\r\n<li>Hi speed USB 2.0 connectivity.</li>\r\n</ul>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `productcategory`
--

CREATE TABLE `productcategory` (
  `ID` int(11) NOT NULL,
  `productCategory` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productcategory`
--

INSERT INTO `productcategory` (`ID`, `productCategory`) VALUES
(3, 'storages'),
(4, 'desktops'),
(5, 'laptops'),
(6, 'laptop accessories'),
(7, 'printers'),
(8, 'pos & retail products'),
(9, 'telephone & pabx'),
(11, 'firewall & vpn products'),
(12, 'wireless accespoints'),
(13, 'routers'),
(14, 'switches'),
(23, 'SERVERS');

-- --------------------------------------------------------

--
-- Table structure for table `tbllogin`
--

CREATE TABLE `tbllogin` (
  `ID` int(11) NOT NULL,
  `userName` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(200) NOT NULL DEFAULT '0',
  `type` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbllogin`
--

INSERT INTO `tbllogin` (`ID`, `userName`, `password`, `type`) VALUES
(1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `productcategory`
--
ALTER TABLE `productcategory`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbllogin`
--
ALTER TABLE `tbllogin`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `productcategory`
--
ALTER TABLE `productcategory`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tbllogin`
--
ALTER TABLE `tbllogin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
